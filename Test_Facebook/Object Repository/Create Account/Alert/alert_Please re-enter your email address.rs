<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_Please re-enter your email address</name>
   <tag></tag>
   <elementGuidId>fd8a180a-7414-4fb8-bb6c-32712c85ec64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#js_13o</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*/text()[normalize-space(.)='Please re-enter your email address.']/parent::*</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='js_1av']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Please re-enter your email address.']/parent::*</value>
      <webElementGuid>4ab33b77-e857-4db3-8ed5-c699bf71896f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='js_13o']</value>
      <webElementGuid>7554adf7-678d-4123-9d50-7349d8f6f15b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//html[@id='facebook']/body/div[10]/div/div/div</value>
      <webElementGuid>a3b9fdc4-cb46-4f56-991d-56559ec8c992</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('You', &quot;'&quot;, 'll use this when you log in and if you ever need to reset your password.')])[1]/following::div[4]</value>
      <webElementGuid>afb4e254-1885-4465-9466-2b23471f19d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('What', &quot;'&quot;, 's your name?')])[2]/following::div[8]</value>
      <webElementGuid>ec191bfa-3c9f-42d7-b3a4-3129630ba8a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Please re-enter your email address.']/parent::*</value>
      <webElementGuid>5c2ecaf2-1bfd-475f-8647-fc363d5029d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]/div/div/div</value>
      <webElementGuid>b10ecf9c-2410-4255-9013-9e91382841c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'js_13o' and (text() = 'Please re-enter your email address.' or . = 'Please re-enter your email address.')]</value>
      <webElementGuid>370e4a73-b927-4cec-80a3-3498a6fe8444</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
