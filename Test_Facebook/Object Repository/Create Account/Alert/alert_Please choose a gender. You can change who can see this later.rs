<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_Please choose a gender. You can change who can see this later</name>
   <tag></tag>
   <elementGuidId>b88b98bd-7335-4318-ae96-cb5041e98814</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#js_2pe</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*/text()[normalize-space(.)='Please choose a gender. You can change who can see this later.']/parent::*</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='js_2pe']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Please choose a gender. You can change who can see this later.']/parent::*</value>
      <webElementGuid>29743c27-6cdf-40e2-a92e-419aca8a2534</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='js_2pe']</value>
      <webElementGuid>54e6422c-7cc0-4841-a39b-a7e274b27c7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//html[@id='facebook']/body/div[14]/div/div/div</value>
      <webElementGuid>7f7fd7bb-980e-4594-98b6-bf859a90638e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please enter your age.'])[1]/following::div[8]</value>
      <webElementGuid>b9d3b41a-783c-4d61-a99e-823cc7f0b7d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter a combination of at least six numbers, letters and punctuation marks (such as ! and &amp;).'])[1]/following::div[12]</value>
      <webElementGuid>9e77ca7b-8ef9-42bb-be7d-6236ae5f213c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Please choose a gender. You can change who can see this later.']/parent::*</value>
      <webElementGuid>f0cda819-c6b3-4a30-b496-85357ef586fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[14]/div/div/div</value>
      <webElementGuid>7ee3f85b-acf4-4273-8dd6-a791a26ddda6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'js_2pe' and (text() = 'Please choose a gender. You can change who can see this later.' or . = 'Please choose a gender. You can change who can see this later.')]</value>
      <webElementGuid>9e3015a3-698c-45eb-8d6d-ec95a5b7c9b6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
