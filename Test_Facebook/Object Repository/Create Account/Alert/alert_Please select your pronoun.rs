<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_Please select your pronoun</name>
   <tag></tag>
   <elementGuidId>725e4ad2-23b5-4611-a37b-b1763af9b439</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#js_2z6</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*/text()[normalize-space(.)='Please select your pronoun.']/parent::*</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='js_2z6']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Please select your pronoun.']/parent::*</value>
      <webElementGuid>693cedbb-b7cb-4ef7-9950-c316984df645</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='js_2z6']</value>
      <webElementGuid>40ebc948-94c2-4521-9fcc-fc8e774c94cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//html[@id='facebook']/body/div[15]/div/div/div</value>
      <webElementGuid>ea51daec-50f3-4e79-8089-18320601ba04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please choose a gender. You can change who can see this later.'])[1]/following::div[4]</value>
      <webElementGuid>f06ecb4d-2f36-4d08-af89-ba1b0b5d5e12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please enter your age.'])[1]/following::div[12]</value>
      <webElementGuid>f2d5add9-8f85-435d-9990-fe2aa4c52be6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Please select your pronoun.']/parent::*</value>
      <webElementGuid>d1f5e501-a93d-4cb8-9e58-605dd744ffad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[15]/div/div/div</value>
      <webElementGuid>a0e3c46b-6f4e-4b2b-9da2-66f36be5d934</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'js_2z6' and (text() = 'Please select your pronoun.' or . = 'Please select your pronoun.')]</value>
      <webElementGuid>56f3688a-510a-464d-b714-d255ffa98d97</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
