<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>alert_Enter a combination of at least six numbers, letters and punctuation marks (such as  and )</name>
   <tag></tag>
   <elementGuidId>67ab5e87-e427-4fe2-bf85-79a0eacf2c9f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#js_1da</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*/text()[normalize-space(.)='Enter a combination of at least six numbers, letters and punctuation marks (such as ! and &amp;).']/parent::*</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='js_1da']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Enter a combination of at least six numbers, letters and punctuation marks (such as ! and &amp;).']/parent::*</value>
      <webElementGuid>387f93c7-7c80-403d-9c40-c546f71dcbb6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='js_1da']</value>
      <webElementGuid>c8f8b0d8-04a4-444e-9a2d-134989e1323b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//html[@id='facebook']/body/div[11]/div/div/div</value>
      <webElementGuid>33580c2a-faaa-4df2-a28f-cd6486d2891f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please re-enter your email address.'])[1]/following::div[4]</value>
      <webElementGuid>08112877-841b-422e-bac0-3cec39b88f87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('You', &quot;'&quot;, 'll use this when you log in and if you ever need to reset your password.')])[1]/following::div[8]</value>
      <webElementGuid>dcbfc58a-79b3-4b59-a21c-8715e1edf7f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Enter a combination of at least six numbers, letters and punctuation marks (such as ! and &amp;).']/parent::*</value>
      <webElementGuid>31ac170d-e8cc-45dc-b65c-c6c131f0dc96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/div/div/div</value>
      <webElementGuid>50e197a6-a1f0-4e46-b418-28de8040c8c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'js_1da' and (text() = 'Enter a combination of at least six numbers, letters and punctuation marks (such as ! and &amp;).' or . = 'Enter a combination of at least six numbers, letters and punctuation marks (such as ! and &amp;).')]</value>
      <webElementGuid>ab7f088e-7120-4343-88a6-2406232b6aa7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
