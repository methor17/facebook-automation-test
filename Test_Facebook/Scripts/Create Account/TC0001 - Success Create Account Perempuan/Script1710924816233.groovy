import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.click(findTestObject('Create Account/a_button_create_account'))

WebUI.setText(findTestObject('Create Account/input_nama_depan'), 'Anissa Alya')

WebUI.setText(findTestObject('Create Account/input_nama_belakang'), 'Syafira')

WebUI.setText(findTestObject('Create Account/input_email'), 'anissaalya25@gmail.com')

WebUI.setText(findTestObject('Create Account/input_ulang_email'), 'anissaalya25@gmail.com')

WebUI.setText(findTestObject('Create Account/input_kata_sandi_baru'), 'B4L33ND4H')

WebUI.selectOptionByValue(findTestObject('Create Account/select_tanggal'), '28', false)

WebUI.selectOptionByValue(findTestObject('Create Account/select_bulan'), '8', false)

WebUI.selectOptionByValue(findTestObject('Create Account/select_tahun'), '1998', false)

WebUI.click(findTestObject('Create Account/radiocheck_perempuan'))

//WebUI.click(findTestObject('Create Account/button_daftar'))

