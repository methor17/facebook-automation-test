import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.click(findTestObject('Create Account/a_button_create_account'))

WebUI.click(findTestObject('Create Account/button_daftar'))

WebUI.verifyElementPresent(findTestObject('Create Account/Alert/alert_Whats your first name'), 0)

WebUI.setText(findTestObject('Create Account/input_nama_depan'), 'cascc')

WebUI.verifyElementPresent(findTestObject('Create Account/Alert/alert_Whats your sure name'), 0)

WebUI.setText(findTestObject('Create Account/input_nama_belakang'), 'casasvasv')

WebUI.verifyElementPresent(findTestObject('Create Account/Alert/alert_Youll use this when you log in and if you ever need to reset your password'), 
    0)

WebUI.setText(findTestObject('Create Account/input_email'), 'ascsaca@gmail.com')

WebUI.verifyElementPresent(findTestObject('Create Account/Alert/alert_Please re-enter your email address'), 0)

WebUI.setText(findTestObject('Create Account/input_ulang_email'), 'ascsaca@gmail.com')

WebUI.verifyElementPresent(findTestObject('Create Account/Alert/alert_Enter a combination of at least six numbers, letters and punctuation marks (such as  and )'), 
    0)

WebUI.setText(findTestObject('Create Account/input_kata_sandi_baru'), 'B4NDUNg*')

WebUI.verifyElementPresent(findTestObject('Create Account/Alert/alert_It looks like youve entered the wrong info. Please make sure that you use your real date of birth'), 
    0)

WebUI.selectOptionByValue(findTestObject('Create Account/select_tanggal'), '28', false)

WebUI.selectOptionByValue(findTestObject('Create Account/select_bulan'), '5', false)

WebUI.selectOptionByValue(findTestObject('Create Account/select_tahun'), '1995', false)

WebUI.verifyElementPresent(findTestObject('Create Account/Alert/alert_Please choose a gender. You can change who can see this later'), 
    0)

WebUI.click(findTestObject('Create Account/radiocheck_kelamin_khusus'))

WebUI.verifyElementPresent(findTestObject('Create Account/Alert/alert_Please select your pronoun'), 0)

WebUI.selectOptionByValue(findTestObject('Create Account/input_kelamin_optional'), '6', false)

